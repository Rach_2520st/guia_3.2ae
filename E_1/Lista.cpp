#include <iostream>
#include<stdio.h>
#include <string>
using namespace std;
#include "Lista.h"

Lista::Lista(){}

Nodo* Lista::crear(int nro){

	Nodo *tmp;
	Nodo *actual;
	Nodo *siguiente;

	tmp = new Nodo;
	tmp->nro = nro;
	if (this->raiz == NULL) { 
        this->raiz = tmp;
        tmp->sgte = NULL;
    }else {
		actual = this->raiz;
        while( actual != NULL ){
			siguiente = actual->sgte;
			if(tmp->nro <= actual->nro){
				tmp->sgte = this->raiz;
				this->raiz = tmp;
				break;
			}
			else if(actual->nro < tmp->nro and siguiente == NULL){
				actual->sgte= tmp;
				tmp->sgte=NULL;
				this->ultimo = tmp;
				break;	
			}
			else if(actual->nro < tmp->nro and siguiente->nro > tmp->nro){
				actual->sgte = tmp;
				tmp->sgte = siguiente;
				break;
			}else{
				actual = actual->sgte;
			}
		}
    }
    return this ->raiz;
    
}



void Lista::imprimir(){
	Nodo *tmp = this->raiz;

    while (tmp != NULL) {
		cout << tmp->nro << endl;
		cout << endl;

        
        tmp = tmp->sgte;
    }
}	

Nodo* Lista::get_ultimo(){
	return this->ultimo;
}