#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

typedef struct _Nodo {
    int nro;
    struct _Nodo *sgte;
} Nodo;

class Lista{
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
			
		Lista();

		Nodo* crear(int nro);
	
		void imprimir();
	
		Nodo* get_ultimo();
}; 
#endif
