#include <iostream>
#include <list>
#include<stdio.h>
using namespace std;

#include "Lista.h"

void imprimir_lista(Nodo* tmp){
	
    while (tmp != NULL) {
		cout << tmp->nro << endl;
        
        tmp = tmp->sgte;
    }
    cout << endl;
}

int main(void){
	int largo;
	int nro;
	cout << "¡Bienvenido!" << endl;
	cout << "Este es un programa que crea una lista" << endl;
	cout << "Luego de ingresar los numeros (negativos y positivos) los separa y ordena" << endl;
	cout << "Ingrese cuantos datos quiere ingresar a la lista" << endl;
	cin >> largo;
	Lista *lista = new Lista();
	//lista que contendrá a  los positivos
	Lista *listap = new Lista();
	//lista que contendrá los negativos
	Lista *listan = new Lista();

	Nodo *c = new Nodo();
	Nodo *p = new Nodo();
	Nodo *n = new Nodo();
	
	for (int i = 0; i < largo; ++i)
	{
		int n;
		cout << "Ingrese un numero a su lista" << endl;
		cin >> nro;
		cout << endl;
		c=lista->crear(nro);

	}

	cout << "Lista ingresada originalmente" << endl;
	lista->imprimir();

	while(c != NULL){
		if(c->nro < 0){
			n=listan->crear(c->nro);
		}
		
		else if(c->nro > 0){
			p=listap->crear(c->nro);
		}
		c = c->sgte;
	}

	cout << "Lista de números negativos" << endl;
	imprimir_lista(n);
	cout  << "Lista de números  positivos" << endl;
	imprimir_lista(p);
	return 0;
}