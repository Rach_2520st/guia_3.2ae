Lista de números negativos y positivos:

Este es un programa que se encarga de generar un lista ingresada por el usuario y a partir de la lista generada por el usuario el programa genera dos listas ordenadas, una de los números negativos y otra de los números positivos.

Este programa contiene una clase denominada lista que se encarga de crear e imprimir la primera lista que se le es entregada, para generar las dos listas siguientes el programa trabaja con los nodos y con una función generada en programa que le permite imprimir la lista pero utilizando el método crear de la clase Lista.
