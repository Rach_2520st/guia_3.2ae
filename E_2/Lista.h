#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

typedef struct _Nodo {
    string nombre;
    struct _Nodo *sgte;
} Nodo;

class Lista{
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
			
		Lista();

		void crear(string nombre);
		void imprimir();
	
}; 
#endif
