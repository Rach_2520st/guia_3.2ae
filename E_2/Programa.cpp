#include <iostream>
#include <list>
#include<stdio.h>
using namespace std;

#include "Lista.h"


int main(void){
	string nombre;
	int opcion;
	cout << "¡Bienvenido!" << endl;
	cout << "Este es un programa que crea una lista enlazada de nombres" << endl;
	
	Lista *lista = new Lista();
	
	while(opcion != 1){
		cout << "Ingrese un nombre a tu lista" << endl;
		getline(cin, nombre);
		lista->crear(nombre);
		cout << "Si quiere seguir ingresando nombres ingrese cualquier número menos el 1" << endl;
		cout << "si quiere detener el programa presione 1" << endl;
		cin >> opcion;
	}
	
	
	return 0;
}