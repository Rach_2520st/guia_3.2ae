#include <iostream>
#include<stdio.h>
#include <string>
using namespace std;
#include "Ingrediente.h"

Ingrediente::Ingrediente(){}

void Ingrediente::crear(string ingrediente){

	Nodo2 *tmp;

    // crea un Nodo2
    tmp = new Nodo2;
    
    tmp->ingrediente = ingrediente;
    
    tmp->ini = NULL;

    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    } else {
        this->ultimo->ini = tmp;
        this->ultimo = tmp;
    }

	imprimir();
	
}


void Ingrediente::imprimir(){
	Nodo2 *tmp = this->raiz;

    while (tmp != NULL) {
		cout << tmp->ingrediente << "-";

        
        tmp = tmp->ini;
    }
}