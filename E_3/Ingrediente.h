#include <iostream>
using namespace std;

#ifndef INGREDIENTE_H
#define INGREDIENTE_H

typedef struct _Nodo2 {
    string ingrediente;
    struct _Nodo2 *ini;
} Nodo2;

class Ingrediente{
	private:
		Nodo2 *raiz = NULL;
		Nodo2 *ultimo = NULL;

	public:
			
		Ingrediente();

		void crear(string ingrediente);
		void imprimir();
	
}; 
#endif